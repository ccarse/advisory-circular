(ns lemondronor.advisorycircular.logging)

(defmacro deflog [service name]
  `(do
     (def ~name (logging/get-logger ~service))
     (let [log# (.bind (.-log ~name) ~name)]
       (defn ~'log-debug [& args#] (apply log# "debug" args#))
       (defn ~'log-verbose [& args#] (apply log# "verbose" args#))
       (defn ~'log-warn [& args#] (apply log# "warn" args#))
       (defn ~'log-info [& args#] (apply log# "info" args#))
       (defn ~'log-error [& args#] (apply log# "error" args#)))))
