;; Simple interface to the pelias geocoder/reverse geocoder.

(ns lemondronor.advisorycircular.pelias
  (:refer-clojure :exclude [reverse])
  (:require
   [cemerick.url :as c-url]
   [kitchen-async.promise :as p]
   [lemondronor.advisorycircular.logging :as logging]
   [lemondronor.advisorycircular.util :as util]))

(declare logger log-debug log-verbose log-info log-warn log-error)
(logging/deflog "pelias" logger)


(def base-pelias-url "http://lockheed.local:4000/v1")

(defn now []
  (.getTime (js/Date.)))


;; Does an HTTP GET to a pelias API url. Returns a promise that
;; resolves to the API results.

(defn pelias-get [base-url endpoint options]
  (p/let [json-str (util/http-get (c-url/url base-url endpoint) options)]
    (let [x (js->clj (.parse js/JSON json-str) :keywordize-keys true)]
      x)))


;; Performs a pelias "nearby" query. Returns a promsie that resolves
;; to the query results.

(defn nearby
  ([config lat lon options]
   (log-verbose "Performing nearby query %s %s %s" lat lon options)
   (p/let [start (now)
           result (pelias-get (:url config) "nearby"
                              {:query (assoc options
                                             :point.lat lat
                                             :point.lon lon)})
           end (now)]
     (log-info "Nearby geo query took %s ms" (- end start))
     result)))


;; Performs a pelias "reverse" query. Retuns a promise that resolves
;; to the query result.

(defn reverse
  ([config lat lon options]
   (log-verbose "Performing reverse query %s %s %s" lat lon options)
   (p/let [start (now)
           result (pelias-get (:url config) "reverse"
                              {:query (assoc options
                                             :point.lat lat
                                             :point.lon lon)})
           end (now)]
     (log-info "Reverse geo query took %s ms" (- end start))
     result)))
