(ns lemondronor.advisorycircular.generation-test
  (:require [cljs.test :refer (deftest is testing)]
            [lemondronor.advisorycircular.generation :as generation]))

(deftest parse-template
  (is (= [:optional [:varref "woo"]]
         (generation/parse-template "?:{woo}")))
  (is (= [:sequence [:text "Hello "] [:varref "foo"] [:text "!"]]
         (generation/parse-template "Hello {foo}!")))
  (is (= [:sequence
          [:text "Hi "] [:varref "name"] [:text ", "]
          [:optional [:varref "woo"]]
          [:text " "]
          [:choice
           [:text "What up?"]
           [:text "Seeya"]]]
         (generation/parse-template "Hi {name}, ?:{woo} [What up?|Seeya]")))
  (is (= [:sequence
          [:text "Hi "] [:varref "name"] [:text ", "]
          [:optional
           [:sequence [:varref "woo"] [:text " "]]]
          [:choice [:text "What up?"] [:text "Seeya"]]]
         (generation/parse-template "Hi {name}, ?:[{woo} ][What up?|Seeya]"))))

(deftest generate
  (is (= ["Hello!"]
         (generation/generate-all
          [(generation/parse-template "Hello!")]
          {})))
  (is (= "Hello!"
         (generation/generate
          [(generation/parse-template "Hello!")]
          {}))))

(deftest generate-test
  (is (= [{:varrefs [], :text ""} {:varrefs [], :text "woo"}]
         (generation/expand%
          (generation/parse-template "?:woo"))))
  (is (= [{:varrefs [], :text ""} {:varrefs [], :text "woo bar"}]
         (generation/expand%
          (generation/parse-template "?:[woo bar]"))))
  (is (= [{:varrefs [], :text ""} {:varrefs [:woo], :text "WOO"}]
         (generation/expand%
          (generation/parse-template "?:{woo}")
          {:woo "WOO"})))
  (is (= ["Hi Tim, What up?"
          "Hi Tim, Seeya"
          "Hi Tim, WOO! What up?"
          "Hi Tim, WOO! Seeya"]
         (map :text
              (generation/expand%
               (generation/parse-template "Hi {name}, ?:[{woo} ][What up?|Seeya]")
               {:name "Tim" :woo "WOO!"})))))

(deftest generate-scoring-with-weights
  (is (= ["Neighborhood"
          "Locality Region Country"]
         (map :text
              (generation/expand%
               (generation/parse-template "[{neighborhood}|{locality} {region} {country}]")
               {:neighborhood "Neighborhood"
                :locality "Locality"
                :region "Region"
                :country "Country"}
               {:neighborhood 5
                :locality 2})))))


(deftest is-consonant
  (is (generation/is-consonant? "f"))
  (is (generation/is-consonant? "F"))
  (is (not (generation/is-consonant? "a")))
  (is (not (generation/is-consonant? "A"))))


(deftest filters
  (testing "a-an"
    (let [templates [(generation/parse-template "It is {thing|a-an}")]]
      (is (= "It is a bird"
             (generation/generate templates {:thing "bird"})))
      (is (= "It is an animal"
             (generation/generate templates {:thing "animal"})))
      (is (= "It is an Animal"
             (generation/generate templates {:thing "Animal"})))
      (is (= "It is a Eurocopter"
             (generation/generate templates {:thing "Eurocopter"})))
      (is (= "It is an MD-500"
             (generation/generate templates {:thing "MD-500"})))
      (is (= "It is an F-15"
             (generation/generate templates {:thing "F-15"})))
      (is (= "It is an X-15"
             (generation/generate templates {:thing "X-15"})))
      (is (= "It is a KXL-10"
             (generation/generate templates {:thing "KXL-10"})))

      )))

(deftest var-paths
  (testing "var-paths"
    (is (= "Hello, John"
           (generation/generate
            [(generation/parse-template "Hello, {person.name}")]
            {:person {:name "John"}})))
    (is (= "I am 49"
           (generation/generate
            [(generation/parse-template "I am {person.age}")]
            {:person {:age 49}})))))
