(ns lemondronor.advisorycircular-test
  (:require [cljs.test :refer (async deftest is testing)]
            [kitchen-async.promise :as p]
            [lemondronor.advisorycircular :as advisorycircular]
            [lemondronor.advisorycircular.pelias :as pelias]))


(def epsilon 0.0000010)

(defn a= [a b]
  (< (Math/abs (- a b)) epsilon))

(defn strmatch [pattern text]
  (if (string? text)
    (re-find pattern text)
    false))


;; (deftest bearing->angle
;;   (is (angles= (cooleradar/bearing->angle 0) (/ Math/PI 2)))
;;   (is (angles= (cooleradar/bearing->angle (/ Math/PI 2)) 0))
;;   (is (angles= (cooleradar/bearing->angle Math/PI) (* 2 (/ 3 4) Math/PI))))

(deftest parse-adsbexchange-ac-element
  (let [ac {"gnd" "0", "trt" "2", "pos" "1", "call" "", "mil" "0", "ttrk" "",
            "dst" "14.6", "reg" "HL7634", "altt" "0", "cou" "South Korea",
            "postime" "1575488288571", "galt" "139", "mlat" "0", "spd" "10.5",
            "sqk" "", "talt" "", "wtc" "3", "alt" "100", "lon" "-118.416438",
            "opicao" "AAR", "interested" "0", "trak" "264.4", "type" "A388",
            "trkh" "0", "icao" "71BE34", "lat" "33.937908", "vsit" "1",
            "tisb" "0", "vsi" "0", "sat" "0"}]
    (is (= (advisorycircular/parse-adsbexchange-ac-element ac)
           {:icao "71BE34"
            :registration "HL7634"
            :callsign nil
            :type "A388"
            :lon -118.416438
            :lat 33.937908
            :alt 100
            :speed 10.5
            :squawk nil
            :military? false
            :mlat? false
            :postime 1575488288571}))))


(deftest prune-history
  (let [hist [{:time 0 :id 0} {:time 1000000 :id 1} {:time 2000000 :id 2}]]
    (is (= (advisorycircular/prune-history hist 2500000 advisorycircular/default-config)
           [{:time 2000000 :id 2}]))))


(deftest update-history-db-record
  (testing "Updating an existing record"
    (let [db  {"0" {:icao "0"
                    :lat 0
                    :lon 0
                    :postime 3000
                    :history [{:time 1000 :id 0}
                              {:time 2000 :id 1}
                              {:time 3000  :id 2}]}
               "1" :anything}
          record {:icao "0"
                  :lat 1
                  :lon 1
                  :postime 3500}]
      (is (= (advisorycircular/update-history-db-record db record)
             {"0" {:icao "0"
                   :lat 1
                   :lon 1
                   :postime 3500
                   :history [{:time 1000 :id 0}
                             {:time 2000 :id 1}
                             {:time 3000 :id 2}
                             {:time 3500 :lat 1 :lon 1}]}
              "1" :anything}))))
  (testing "Adding a new record"
    (let [db {"0" {:icao "0"
                   :lat 0
                   :lon 0
                   :postime 3000
                   :history [{:time 1000 :id 0}
                             {:time 2000 :id 1}
                             {:time 3000 :id 2}]}
              "1" :anything}
          record {:icao "2"
                  :lat 1
                  :lon 1
                  :postime 3500}]
      (is (= (advisorycircular/update-history-db-record db record)
             {"0" {:icao "0"
                   :lat 0
                   :lon 0
                   :postime 3000
                   :history [{:time 1000 :id 0}
                             {:time 2000 :id 1}
                             {:time 3000 :id 2}]}
              "1" :anything
              "2" {:icao "2"
                   :lat 1
                   :lon 1
                   :postime 3500
                   :history [{:time 3500 :lat 1 :lon 1}]}})))))


(deftest expand-template
  (let [data {:locality "Palmdale", :continent "North America", :military? true, :alt 3850, :speed "209", :normalized-curviness 14.768651250300287, :accuracy "centroid", :country_a "USA", :continent_gid "whosonfirst:continent:102191575", :name "Palmdale", :squawk "5330", :icao "AE1482", :county_a "LO", :county "Los Angeles County", :source "whosonfirst", :gid "whosonfirst:locality:85923493", :curviness 1269.8089810739468, :locality_gid "whosonfirst:locality:85923493", :region "California", :militaryicao "AE1482", :region_a "CA", :nearbydistance 8.167, :callsign "RAIDR49", :layer "locality", :mlat? false, :country_gid "whosonfirst:country:85633793", :label "Palmdale, CA, USA", :id "85923493", :lon -118.00375, :region_gid "whosonfirst:region:85688637", :lat 34.661074, :militaryregistration "166765", :county_gid "whosonfirst:county:102086957", :started-circling-time 1576266715691, :distance 6.855, :source_id "85923493", :registration "166765", :confidence 0.5, :country "United States", :postime 1576266689756, :nearbylandmark "Living Faith Foursquare Church"}]
    (is (strmatch #"military" (-> (advisorycircular/expand-template data) :text))))
  (let [data {:locality "Palmdale", :continent "North America", :military? true, :alt 3200, :speed "161", :normalized-curviness 15.783422690487765, :accuracy "centroid", :country_a "USA", :continent_gid "whosonfirst:continent:102191575", :name "Palmdale", :squawk "5330", :icao "AE1482", :county_a "LO", :county "Los Angeles County", :source "whosonfirst", :gid "whosonfirst:locality:85923493", :curviness 1098.803548060181, :locality_gid "whosonfirst:locality:85923493", :region "California", :militaryicao "AE1482", :region_a "CA", :nearbydistance 7.828, :callsign "RAIDR49", :layer "locality", :mlat? false, :country_gid "whosonfirst:country:85633793", :label "Palmdale, CA, USA", :id "85923493", :lon -118.049183, :region_gid "whosonfirst:region:85688637", :lat 34.649808, :militaryregistration "166765", :county_gid "whosonfirst:county:102086957", :started-circling-time 1576267564959, :distance 6.336, :source_id "85923493", :registration "166765", :confidence 0.5, :country "United States", :postime 1576267555709, :nearbylandmark "Living Faith Foursquare Church"}]
    (is (strmatch #"military" (-> (advisorycircular/expand-template data) :text))))
  (testing "a vs. an for type"
    (let [data {:registration "TEST" :icao "123" :type "Airbus 380" :locality "Test City"}]
      (is (strmatch #"an Airbus" (:text (advisorycircular/expand-template data)))))
    (let [data {:registration "TEST" :icao "123" :type "Yoyo 380" :locality "Test City"}]
      (is (strmatch #"a Yoyo" (:text (advisorycircular/expand-template data)))))))


(deftest merge-adsbx-sqb
  (is (= (advisorycircular/merge-adsbx-sqb {:registration "N1"}
                                           {:registration "N2" :type "B52"})
         {:registration "N1", :type "B52"}))
  (is (= (advisorycircular/merge-adsbx-sqb {:registration nil}
                                           {:registration "N2" :type "B52"})
         {:registration "N2", :type "B52"}))
  (is (= (advisorycircular/merge-adsbx-sqb {:registration "N1"}
                                           {:registration "N2" :type nil})
         {:registration "N1", :type nil})))


(deftest generate-description
  (testing "Basic generation"
    (let [ac {:icao "B00B00"
              :registration "NBADB0Y"}
          sqb {:registration "NGOODB0Y"
               :type "B52"}
          reverse {:properties {:neighbourhood "Silver Lake" :locality "Los Angeles"}}
          nearby nil
          desc (advisorycircular/generate-description ac sqb reverse nearby)]
      (is (strmatch #"NBADB0Y" desc))
      (is (strmatch #"a B52" desc))
      (is (strmatch #"Silver Lake.*Los Angeles" desc))
      (is (strmatch #"#NBADB0Y" desc))))
  (testing "Missing ADSBX registration"
    (let [ac {:icao "B00B00"}
          sqb {:registration "NGOODB0Y"
               :type "B52"}
          reverse {:properties {:neighbourhood "Silver Lake" :locality "Los Angeles"}}
          nearby {:name "Disneyland" :distance 2}
          desc (advisorycircular/generate-description ac sqb reverse nearby)]
      (is (strmatch #"NGOODB0Y" desc))
      (is (strmatch #"a B52" desc))
      (is (strmatch #"Silver Lake.*Los Angeles" desc))
      (is (strmatch #"#NGOODB0Y" desc))))
  (testing "foo"
    (let [ac {:military? false :alt 1300 :speed 72.1 :squawk "1200"
              :icao "AAE0C2" :type nil, :callsign "N80NT", :registration nil}
          sqb {:registration "N80NT", :type "Eurocopter Squirrel AS 350 B2"}
          reverse {:properties {:neighbourhood "Silver Lake" :locality "Los Angeles"}}
          nearby {:name "Disneyland" :distance 2}
          desc (advisorycircular/generate-description ac sqb reverse nearby)]
      (is (strmatch #"N80NT" desc))
      (is (strmatch #"a Eurocopter Squirrel AS 350 B2" desc))
      (is (strmatch #"callsign N80NT" desc))
      (is (strmatch #"Silver Lake.*Los Angeles" desc))
      (is (strmatch #"1300 feet" desc))
      (is (strmatch #"speed 83 MPH" desc))
      (is (strmatch #"squawking 1200" desc))
      (is (strmatch #"#N80NT" desc)))))


(deftest filter-landmarks
  (testing "filter-landmarks"
    (let [landmarks [{:properties {:name "Johnny Depp"}}
                     {:properties {:name "Musso & Frank's"}}
                     {:properties {:name "Johnny Depp's Star"}}]]
      (is (= (advisorycircular/filter-landmarks {:blocklist ["Johnny Depp"]}
                                                landmarks)
             [{:properties {:name "Musso & Frank's"}}]))
      (is (= (advisorycircular/filter-landmarks {:blocklist ["Frank"]}
                                                landmarks)
             [{:properties {:name "Johnny Depp"}}
              {:properties {:name "Johnny Depp's Star"}}])))))


(def r2508-data1
  [{:continent "North America"
    :accuracy "point"
    :country_a "USA"
    :continent_gid "whosonfirst:continent:102191575"
    :name "Haystack Butte"
    :county_a "SA"
    :county "San Bernardino County"
    :source "openstreetmap"
    :gid "openstreetmap:venue:node/358808120"
    :region "California"
    :region_a "CA"
    :layer "venue"
    :country_gid "whosonfirst:country:85633793"
    :label "Haystack Butte, San Bernardino County, CA, USA"
    :id "node/358808120"
    :region_gid "whosonfirst:region:85688637"
    :county_gid "whosonfirst:county:102085395"
    :distance 4.261
    :source_id "node/358808120"
    :confidence 0.5
    :country "United States"}
   {:continent "North America"
    :military?
    true
    :alt 5825
    :speed "226"
    :normalized-curviness 14.188938867970446
    :accuracy "centroid"
    :country_a "USA"
    :continent_gid "whosonfirst:continent:102191575"
    :name "San Bernardino County"
    :squawk "0026"
    :icao "AE264F"
    :county_a "SA"
    :county "San Bernardino County"
    :type "Beechcraft C-12C Huron"
    :source "whosonfirst"
    :gid "whosonfirst:county:102085395"
    :curviness 1463.4205557421808
    :region "California"
    :militaryicao "AE264F"
    :region_a "CA"
    :nearbydistance "2.65"
    :callsign "COBRA02"
    :layer "county"
    :mlat? false
    :country_gid "whosonfirst:country:85633793"
    :label "San Bernardino County, CA, USA"
    :id "102085395"
    :lon -117.629528
    :region_gid "whosonfirst:region:85688637"
    :lat 34.884804
    :militaryregistration "73-1215?"
    :county_gid "whosonfirst:county:102085395"
    :started-circling-time 1581098223891
    :distance 132.69
    :source_id "102085395"
    :registration "73-1215?"
    :confidence 0.5
    :country "United States",
    :postime 1581098213228
    :nearbylandmark "Haystack Butte"}
   {:continent "North America"
    :military? true
    :alt 14000
    :speed "139"
    :normalized-curviness 6.97888492071943
    :accuracy "centroid"
    :country_a "USA"
    :continent_gid "whosonfirst:continent:102191575"
    :name "Kern County"
    :squawk "0006"
    :icao "AE2651"
    :county_a "KE"
    :county "Kern County"
    :type "Beechcraft C-12C Huron"
    :source "whosonfirst"
    :gid "whosonfirst:county:102081727"
    :curviness 1447.2660076871339
    :region "California"
    :militaryicao "AE2651"
    :region_a "CA"
    :nearbydistance "1.93"
    :callsign "COBRA37"
    :layer "county"
    :mlat? false
    :country_gid "whosonfirst:country:85633793"
    :label "Kern County, CA, USA"
    :id "102081727"
    :lon -117.654247
    :region_gid "whosonfirst:region:85688637"
    :lat 35.076273
    :militaryregistration "76-0166"
    :county_gid "whosonfirst:county:102081727"
    :started-circling-time 1588785019767
    :distance 101.122
    :source_id "102081727"
    :registration "76-0166"
    :confidence 0.5
    :country "United States"
    :postime 1588785014065
    :nearbylandmark "Russell Mine"}])


(deftest r2508-description
  (testing "R-2508 generation"
    (is (= (:text (advisorycircular/expand-template (nth r2508-data1 0)))
           "73-1215?, a military Beechcraft C-12C Huron, (callsign COBRA02) is circling over San Bernardino County at 5825 feet, speed 226 MPH, squawking 0026, 2.65 miles from Haystack Butte #73-1215? https://tar1090.adsbexchange.com/?icao=AE264F&zoom=13"))
    (is (= (:text (advisorycircular/expand-template (nth r2508-data1 1)))
           "73-1215?, a military Beechcraft C-12C Huron, (callsign COBRA02) is circling over San Bernardino County at 5825 feet, speed 226 MPH, squawking 0026, 2.65 miles from Haystack Butte #73-1215? https://tar1090.adsbexchange.com/?icao=AE264F&zoom=13"))
    (is (= (:text (advisorycircular/expand-template (nth r2508-data1 2)))
           "76-0166, a military Beechcraft C-12C Huron, (callsign COBRA37) is circling over Kern County at 14000 feet, speed 139 MPH, squawking 0006, 1.93 miles from Russell Mine #76-0166 https://tar1090.adsbexchange.com/?icao=AE2651&zoom=13")))

  (testing "foo"
    (let [ac {:military? false :alt 1300 :speed 72.1 :squawk "1200"
              :icao "AAE0C2" :type nil, :callsign "N80NT", :registration nil}
          sqb {:registration "N80NT", :type "Eurocopter Squirrel AS 350 B2"}
          reverse {:properties {:neighbourhood "Silver Lake" :locality "Los Angeles"}}
          nearby {:name "Disneyland" :distance 2}
          desc (advisorycircular/generate-description ac sqb reverse nearby)]
      (is (strmatch #"N80NT" desc))
      (is (strmatch #"a Eurocopter Squirrel AS 350 B2" desc))
      (is (strmatch #"callsign N80NT" desc))
      (is (strmatch #"Silver Lake.*Los Angeles" desc))
      (is (strmatch #"1300 feet" desc))
      (is (strmatch #"speed 83 MPH" desc))
      (is (strmatch #"squawking 1200" desc))
      (is (strmatch #"#N80NT" desc)))))


(deftest closest-airport
  (let [a1 {:properties {:distance 1.5 :label "Bridge heliport"}}
        a2 {:properties {:distance 0.5 :label "New York Seaport"}}
        a3 {:properties {:distance 1.0 :label "LAX"}}
        nearby (fn [config lat lon options]
                 {:features [a1 a2 a3]})]
    (async
     done
     (p/do
       (testing "closest-airport 1"
         ;; Note that the with-redefs only works for the first
         ;; binding clause in p/let.
         (with-redefs [pelias/nearby nearby]
           (p/let [r (advisorycircular/closest-airport {} 0 0)]
             (is (= r a2)))))
       (testing "closest-airport with blocklist"
         (with-redefs [pelias/nearby nearby]
           (let [conf {:airport {:blocklist ["seaport"]}}]
             (p/let [r (advisorycircular/closest-airport conf 0 0)]
               (is (= r a3))))))
       (done))

     )))
